# fe-interview-task
You are a new FE developer assigned a BUG ticket in Jira, your goal is to identify and fix the described issues, afterwards creating a merge request on GitLab.

- Resolve the issues described in the ticket
- Write a unit test ensuring the bug will not appear in the future (bonus)
- Create a merge request

## Jira ticket
![jira ticket](doc/jira.jpg)

## API endpoint for site types
https://mocki.io/v1/a0b71662-473d-4445-8d38-d991cd91e757
> :warning: Please slack `@Linas Ramanauskas` or `@Aurelija P` if you face issues with API.

## Running the app locally
In order to complete the task, you will have to clone the repository, install dependencies and run the app locally.

### Pre-requisites
1. Ensure Node.js (v18+) is installed https://nodejs.org/en, if you use multiple different versions of node for different projects set up nvm https://github.com/nvm-sh/nvm#installing-and-updating
2. Ensure yarn is installed `npm i -g yarn`
3. Ensure git is setup on your local machine https://git-scm.com/downloads
4. If you don't have a GitLab account, create one https://gitlab.com/users/sign_up

### Installing dependencies and running locally
1. Clone repo either with SSH `git@gitlab.com:linas.ramanauskas/fe-interview-task.git` (https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) or HTTPS `https://gitlab.com/linas.ramanauskas/fe-interview-task.git`
2. Install dependencies `cd fe-interview-task && yarn install`
3. Run the project in dev mode `yarn dev`, run tests `yarn test`

## Submitting your work
After completing the task you will have to open a merge request for review.

1. Create a bugfix branch `git checkout -b bug/<JIRA_TICKET_ID>`
2. Commit and push your changes `git add . && git commit -m "<DESCRIPTION>" && git push`
3. Open a merge request https://gitlab.com/linas.ramanauskas/fe-interview-task/-/merge_requests

