import { render } from "@test";
import Header from "./Header";

describe("components/Header", () => {
  it("should render the component correctly and match snapshot", () => {
    const { container } = render(<Header />);
    expect(container).toMatchSnapshot();
  });

  it("should redirect to correct pages when Links are clicked", async () => {
    const { user, findByText } = render(<Header />);
    await user.click(await findByText(/Home/));
    expect(window.location.pathname).toEqual("/");
  });
});
