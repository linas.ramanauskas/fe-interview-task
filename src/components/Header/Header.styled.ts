import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const LinksContainer = styled.div`
  display: flex;
  margin-left: 2em;
  gap: 1em;
`;

export const Link = styled(NavLink)`
  color: ${({ theme }) => theme.palette.common.white};
  text-transform: uppercase;
  text-decoration: none;
  text-underline-offset: 0.2em;
  &:hover,
  &.active {
    text-decoration: underline;
  }
`;
