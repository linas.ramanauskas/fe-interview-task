import { AppBar, Toolbar, Typography } from "@mui/material";
import { LinksContainer, Link } from "./Header.styled";
import { Colors } from "@globalStyles";

function Header() {
  return (
    <AppBar position="static" sx={{ backgroundColor: Colors.primary }}>
      <Toolbar>
        <Typography variant="h6">fe-interview-task</Typography>
        <LinksContainer>
          <Link to="/">Home</Link>
        </LinksContainer>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
