import { createGlobalStyle } from "styled-components";

export const Colors = {
  primary: "#44878c",
  secondary: "#424242",
};

const GlobalStyles = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
    font-family: Roboto;
    height: 100%;
  }
  #root {
    height: 100%;
  }
`;

export default GlobalStyles;
