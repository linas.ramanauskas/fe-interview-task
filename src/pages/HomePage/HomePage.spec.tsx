import { render } from "@test";
import HomePage from "./HomePage";

describe("components/HomePage", () => {
  it("should render the component correctly and match snapshot", () => {
    const { container } = render(<HomePage />);
    expect(container).toMatchSnapshot();
  });
});
