import { Colors } from "@globalStyles";
import { TextField, Autocomplete, Button } from "@mui/material";
import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
`;

export const Form = styled.form`
  display: grid;
  gap: 2rem;
  grid-template-columns: repeat(4, 1fr);
`;

export const Input = styled(TextField)`
  & .Mui-focused {
    &.MuiFormLabel-root {
      color: ${Colors.primary};
    }
  }
`;

export const StyledAutocomplete = styled(Autocomplete)`
  & .Mui-focused {
    &.MuiFormLabel-root {
      color: ${Colors.primary};
    }

    &.MuiInputBase-root,
    & .MuiOutlinedInput-notchedOutline {
      border-color: ${Colors.primary};
    }
  }
`;

export const PlaceholderInput = styled(TextField)`
  ::placeholder {
    color: ${Colors.secondary};
  }

  & .Mui-focused {
    &.MuiFormLabel-root {
      color: ${Colors.primary};
    }
  }
`;

export const StyledButton = styled(Button)`
  color: ${Colors.primary};
  border-color: ${Colors.primary};

  &:hover {
    border-color: ${Colors.primary};
  }
`;
