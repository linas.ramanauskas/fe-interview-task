import { useState } from "react";
import { useMutation } from "react-query";
import { Alert, Backdrop, CircularProgress, TextField } from "@mui/material";
import {
  MainContainer,
  Form,
  Input,
  StyledAutocomplete,
  PlaceholderInput,
  StyledButton,
} from "./HomePage.styled";
import { callDummyPost } from "./apiService";

const dummyOptions = ["Option 1", "Option 2"];

function HomePage() {
  const [isSuccess, setIsSuccess] = useState(false);

  const { mutate: postSite, isLoading: isLoadingPostSite } = useMutation({
    mutationFn: callDummyPost,
    onSuccess: () => {
      setIsSuccess(true);
    },
  });

  return (
    <MainContainer>
      <Form>
        <Input id="site-name" label="Site name" />
        <StyledAutocomplete
          renderInput={(params) => (
            <TextField
              {...params}
              variant="outlined"
              name="site-type"
              label="Site type"
            />
          )}
          options={dummyOptions}
        />
        <PlaceholderInput
          id="contact-person"
          label="Contact person"
          placeholder="Name Surname"
        />
        <StyledButton
          variant="outlined"
          onClick={() => {
            postSite();
          }}
        >
          Submit
        </StyledButton>
      </Form>
      {isSuccess && (
        <Alert
          severity="success"
          onClose={() => {
            setIsSuccess(false);
          }}
        >
          Site created successfully
        </Alert>
      )}
      {isLoadingPostSite && (
        <Backdrop open>
          <CircularProgress color="success" />
        </Backdrop>
      )}
    </MainContainer>
  );
}

export default HomePage;
