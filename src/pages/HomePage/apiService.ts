export const callDummyPost = async (): Promise<
  Array<{ body: string; id: number; title: string; userId: number }>
> => {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (response.status !== 200) {
    throw new Error("errors.fetching.duplicateProject");
  }
  return await response.json();
};
