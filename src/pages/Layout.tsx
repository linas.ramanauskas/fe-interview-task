import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";
import Header from "../components/Header/Header";

function Layout() {
  return (
    <Box display="flex" flexDirection="column" height="100%">
      <Header />
      <Box display="flex" height="100%" sx={{ p: "2rem 1.5rem" }}>
        <Outlet />
      </Box>
    </Box>
  );
}

export default Layout;
