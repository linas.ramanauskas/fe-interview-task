import { render, RenderOptions } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { QueryClient, QueryClientProvider } from "react-query";
import {
  ThemeProvider as MuiThemeProvider,
  StyledEngineProvider,
  createTheme,
} from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";

const theme = createTheme();

// eslint-disable-next-line react-refresh/only-export-components
const WithProviders = ({
  children,
}: {
  children: React.ReactNode;
  route?: string;
}) => {
  return (
    <StyledEngineProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <BrowserRouter>{children}</BrowserRouter>
        </ThemeProvider>
      </MuiThemeProvider>
    </StyledEngineProvider>
  );
};

const customRender = (
  ui: React.ReactElement,
  options?: { options?: Omit<RenderOptions, "wrapper"> },
) => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
        cacheTime: Infinity,
      },
    },
  });
  const user = userEvent.setup();
  const renderResult = render(
    <WithProviders>
      <QueryClientProvider client={queryClient}>{ui}</QueryClientProvider>
    </WithProviders>,
    {
      ...options?.options,
    },
  );
  return { ...renderResult, user };
};

// eslint-disable-next-line react-refresh/only-export-components
export * from "@testing-library/react";

export { customRender as render };
