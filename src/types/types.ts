import { QueryCache, MutationCache, Logger, DefaultOptions } from "react-query";

export interface QueryClientConfig {
  queryCache?: QueryCache;
  mutationCache?: MutationCache;
  logger?: Logger;
  defaultOptions?: DefaultOptions;
}
