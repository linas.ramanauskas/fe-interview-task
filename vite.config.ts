import { resolve } from "path";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@test": resolve(__dirname, "./src/testUtils.tsx"),
      "@globalStyles": resolve(__dirname, "./src/global.styled.ts"),
    },
  },
});
