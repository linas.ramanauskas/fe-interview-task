import { resolve } from "path";
import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "./setupTests.ts",
  },
  resolve: {
    alias: {
      "@test": resolve(__dirname, "./src/testUtils.tsx"),
      "@globalStyles": resolve(__dirname, "./src/global.styled.ts"),
    },
  },
});
